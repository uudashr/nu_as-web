package api

type Client struct {
}

func (c *Client) RegisterUser(email, password string) (string, error) {
	return "wow", nil
}

func (c *Client) UserByCredentials(email, password string) (*User, error) {
	return &User{
		ID:       "wow",
		FullName: "John Appleseed",
		Address:  "1 Infinite Loop; Cupertino, CA 95014",
		Email:    "john.appleseed@gmail.com",
		Phone:    "(408) 606-5775",
	}, nil
}

type User struct {
	ID       string
	FullName string
	Address  string
	Email    string
	Phone    string
}
