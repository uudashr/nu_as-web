package http

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"golang.org/x/oauth2"

	"bitbucket.org/uudashr/nu_as-web/internal/api"

	"github.com/gorilla/sessions"

	"github.com/gorilla/mux"
)

const staticPath = "/static/"

type delegate struct {
	tmpl          *template.Template
	cookieStore   *sessions.CookieStore
	googleAuthCfg *oauth2.Config
	client        *api.Client
}

func (d *delegate) indexPage(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/login", http.StatusFound)
}

func (d *delegate) logInPage(w http.ResponseWriter, r *http.Request) {
	if err := d.tmpl.ExecuteTemplate(w, "login.html", nil); err != nil {
		log.Println("Fail to render logInPage:", err)
	}
}

func (d *delegate) logIn(w http.ResponseWriter, r *http.Request) {
	session, err := d.cookieStore.Get(r, "user")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	email := r.PostFormValue("email")
	password := r.PostFormValue("password")
	user, err := d.client.UserByCredentials(email, password)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	session.Values["id"] = user.ID
	if err = session.Save(r, w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/user", http.StatusFound)
}

const oauthStateString = "pseudo-random-asdf"

func (d *delegate) logInGoogle(w http.ResponseWriter, r *http.Request) {
	url := d.googleAuthCfg.AuthCodeURL(oauthStateString)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func (d *delegate) authCallback(w http.ResponseWriter, r *http.Request) {
	state, code := r.FormValue("state"), r.FormValue("code")
	if state != oauthStateString {
		http.Error(w, "invalid oauth state", http.StatusInternalServerError)
		return
	}

	token, err := d.googleAuthCfg.Exchange(context.Background(), code)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	res, err := http.Get(fmt.Sprintf("https://www.googleapis.com/oauth2/v2/userinfo?access_token=%s", token.AccessToken))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer func() {
		if err = res.Body.Close(); err != nil {
			log.Println("Fail to close body:", err)
		}
	}()

	var userInfo googleUserInfo
	if err = json.NewDecoder(res.Body).Decode(&userInfo); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	session, err := d.cookieStore.Get(r, "user")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	session.Values["id"] = "wow"
	session.Values["oauth.accessToken"] = token.AccessToken
	session.Values["oauth.tokenType"] = token.TokenType
	session.Values["oauth.refreshToken"] = token.RefreshToken
	session.Values["oauth.expiry"] = token.Expiry.Unix()
	log.Println("AccessToken:", token.AccessToken)
	if err := session.Save(r, w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/user", http.StatusFound)
}

func (d *delegate) signUpPage(w http.ResponseWriter, r *http.Request) {
	if err := d.tmpl.ExecuteTemplate(w, "signup.html", nil); err != nil {
		log.Println("Fail to render signUpPage:", err)
	}
}

func (d *delegate) signUp(w http.ResponseWriter, r *http.Request) {
	session, err := d.cookieStore.Get(r, "user")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	email := r.PostFormValue("email")
	password := r.PostFormValue("password")
	userID, err := d.client.RegisterUser(email, password)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	session.Values["id"] = userID
	if err = session.Save(r, w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/user/fillProfile", http.StatusFound)
}

func (d *delegate) signUpGoogle(w http.ResponseWriter, r *http.Request) {
	url := d.googleAuthCfg.AuthCodeURL(oauthStateString)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func (d *delegate) fillProfilePage(w http.ResponseWriter, r *http.Request) {
	if err := d.tmpl.ExecuteTemplate(w, "fill_profile.html", nil); err != nil {
		log.Println("Fail to render fillProfilePage:", err)
	}
}

func (d *delegate) fillProfile(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/user", http.StatusFound)
}

func (d *delegate) profilePage(w http.ResponseWriter, r *http.Request) {
	if err := d.tmpl.ExecuteTemplate(w, "profile.html", nil); err != nil {
		log.Println("Fail to render profilePage:", err)
	}
}

func (d *delegate) editProfilePage(w http.ResponseWriter, r *http.Request) {
	if err := d.tmpl.ExecuteTemplate(w, "edit_profile.html", nil); err != nil {
		log.Println("Fail to render editProfilePage:", err)
	}
}

func (d *delegate) updateProfile(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/user", http.StatusFound)
}

func (d *delegate) resetPasswordPage(w http.ResponseWriter, r *http.Request) {
	if err := d.tmpl.ExecuteTemplate(w, "reset_password.html", nil); err != nil {
		log.Println("Fail to render resetPasswordPage:", err)
	}
}

func (d *delegate) resetPassword(w http.ResponseWriter, r *http.Request) {
	if err := d.tmpl.ExecuteTemplate(w, "reset_password_succeed.html", nil); err != nil {
		log.Println("Fail to render resetPassword:", err)
	}
}

func (d *delegate) logout(w http.ResponseWriter, r *http.Request) {
	session, err := d.cookieStore.Get(r, "user")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	accessToken, ok := session.Values["oauth.accessToken"].(string)
	if ok {
		log.Println("Revoking accessToken...")
		res, err := http.Post(fmt.Sprintf("https://accounts.google.com/o/oauth2/revoke?token=%s", accessToken), "application/x-www-form-urlencoded", nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer func() {
			if err = res.Body.Close(); err != nil {
				log.Println("Fail to close:", err)
			}
		}()

		log.Println("Revoking access token response code:", res.StatusCode)
		_, err = io.Copy(ioutil.Discard, res.Body)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	session.Options.MaxAge = -1
	if err = session.Save(r, w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/", http.StatusFound)
}

// NewHandler creates new http.Handler.
func NewHandler(tmpl *template.Template, staticDir http.FileSystem, googleAuthCfg *oauth2.Config) (http.Handler, error) {
	if tmpl == nil {
		return nil, errors.New("empty tmpl")
	}

	cookieStore := sessions.NewCookieStore([]byte("ccd13827-e96a-4fd2-a85e-5eeaf15af21f"))

	d := &delegate{
		tmpl:          tmpl,
		cookieStore:   cookieStore,
		googleAuthCfg: googleAuthCfg,
	}

	r := mux.NewRouter()

	// Index
	r.HandleFunc("/", d.indexPage).Methods(http.MethodGet)

	// Login and signup
	r.HandleFunc("/login", d.logInPage).Methods(http.MethodGet)
	r.HandleFunc("/login", d.logIn).Methods(http.MethodPost)

	r.HandleFunc("/auth/login-google", d.logInGoogle).Methods(http.MethodGet)
	r.HandleFunc("/auth/callback", d.authCallback).Methods(http.MethodGet)

	r.HandleFunc("/signup", d.signUpPage).Methods(http.MethodGet)
	r.HandleFunc("/signup", d.signUp).Methods(http.MethodPost)

	r.HandleFunc("/resetPassword", d.resetPasswordPage).Methods(http.MethodGet)
	r.HandleFunc("/resetPassword", d.resetPassword).Methods(http.MethodPost)

	// User area
	userRoute := r.PathPrefix("/user").Subrouter()
	amw := &authMiddleware{cookieStore: cookieStore}
	userRoute.Use(amw.Middleware)

	userRoute.HandleFunc("/fillProfile", d.fillProfilePage).Methods(http.MethodGet)
	userRoute.HandleFunc("", d.fillProfile).Methods(http.MethodPost)
	userRoute.HandleFunc("", d.profilePage).Methods(http.MethodGet)
	userRoute.HandleFunc("/edit", d.editProfilePage).Methods(http.MethodGet)
	userRoute.HandleFunc("/update", d.updateProfile).Methods(http.MethodPost)
	userRoute.HandleFunc("/logout", d.logout).Methods(http.MethodGet)

	// Static content
	r.PathPrefix(staticPath).Handler(http.StripPrefix(staticPath, http.FileServer(staticDir)))

	return r, nil
}

type googleUserInfo struct {
	ID            string `json:"id"`
	Email         string `json:"email"`
	VerifiedEmail bool   `json:"verified_email"`
	Picture       string `json:"picture"`
}
