package http

import (
	"context"
	"net/http"

	"github.com/gorilla/sessions"
)

type contextKey uint32

const (
	contextKeyUserID contextKey = iota
)

type authMiddleware struct {
	cookieStore *sessions.CookieStore
}

func (m *authMiddleware) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, err := m.cookieStore.Get(r, "user")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		userID, ok := session.Values["id"].(string)
		if !ok {
			// TODO: forbidded, need to login
			http.Redirect(w, r, "/login", http.StatusMovedPermanently)
			return
		}

		ctx := context.WithValue(r.Context(), contextKeyUserID, userID)
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})
}
