SOURCES := $(shell find . -name '*.go' -type f -not -path './vendor/*'  -not -path '*/mocks/*')

# Linter
.PHONY: prepare-lint
prepare-lint:
	@echo Install linter
	@curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b $(go env GOPATH)/bin v1.17.1

.PHONY: lint
lint:
	@echo Lint checks
	@golangci-lint run $(LINT_OPTS)

# Dependency management
.PHONY: dep-upgrade
dep-upgrade:
	@echo Upgrading dependencies
	@go get -u

.PHONY: dep-tidy
dep-tidy:
	@echo Tidying dependencies
	@go mod tidy

# Generate files
.PHONY: generate
generate:
	@echo Generating files
	@go generate ./...

# Testing
.PHONY: test
test:
	@echo Run tests
	@go test $(TEST_OPTS) ./...

test-all:
	@echo Run all tests
	@go test -tags=integration $(TEST_OPTS) ./...

# Build binary
web-up: cmd/$@ $(SOURCES)
	@echo "Building $@"
	@CGO_ENABLED=0 go build -a -installsuffix cgo -o $@ cmd/$@/*.go

# Build docker
.PHONY: docker
docker:
	@echo Building docker
	@docker build -t nuas-web .

# House keeping
.PHONY: clean
clean:
	@echo Cleaning...
	@if [ -f ./web-up ]; then rm ./web-up; fi
