package main

import (
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	nethttp "net/http"
	"path/filepath"

	"bitbucket.org/uudashr/nu_as-web/internal/http"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

func main() {
	flagViewsDir := flag.String("views-dir", "views", "Path to the view templates")
	flagStaticDir := flag.String("static-dir", "static", "Path to static assets")
	flagGoogleClientID := flag.String("google-client-id", "", "Google Client ID")
	flagGoogleSecret := flag.String("google-client-secret", "", "Google Client secret")
	flagBaseURL := flag.String("base-url", "http://localhost:8080", "Base URL")
	flagPort := flag.Int("port", 8080, "HTTP listen port")
	flagHelp := flag.Bool("help", false, "Show help (this message)")

	flag.Parse()

	if *flagHelp {
		flag.Usage()
		return
	}

	// Views
	tmpl, err := loadTemplate(*flagViewsDir)
	if err != nil {
		log.Fatal("Fail to load template:", err)
	}

	// Static files
	staticDir := nethttp.Dir(*flagStaticDir)

	// Google OAuth2
	authCfg := &oauth2.Config{
		RedirectURL:  fmt.Sprintf("%s/auth/callback", *flagBaseURL),
		ClientID:     *flagGoogleClientID,
		ClientSecret: *flagGoogleSecret,
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
		Endpoint:     google.Endpoint,
	}

	// HTTP Handler
	handler, err := http.NewHandler(tmpl, staticDir, authCfg)
	if err != nil {
		log.Fatal("Fail to create http.Handler:", err)
	}

	server := &nethttp.Server{
		Addr:    fmt.Sprintf(":%d", *flagPort),
		Handler: handler,
	}

	log.Printf("Listening on port %d ...\n", *flagPort)
	if err = server.ListenAndServe(); err != nethttp.ErrServerClosed {
		log.Println("Fail to listen and serve:", err)
	}
}

func loadTemplate(templateDir string) (*template.Template, error) {
	files, err := ioutil.ReadDir(templateDir)
	if err != nil {
		return nil, err
	}

	var viewPaths []string
	for _, f := range files {
		p := filepath.Join(templateDir, f.Name())
		viewPaths = append(viewPaths, p)
	}

	return template.ParseFiles(viewPaths...)
}
